# Entregable 1 FC1: Simulación de la Interacción de 2 particulas en presencia de campo magnético



## Que hace el proyecto 
Este proyecto es una simulación diseñada en codigo python para estudiar la interacción entre dos partículas en presencia de un campo magnético. La simulación utiliza algoritmos y librerias que permiten modelar el comportamiento dinámico de las partículas y su respuesta al campo magnético.

## Utilidad
Esta simulación es beneficiosa para investigadores, estudiantes interesados en física de particulas y relacionados. Debido a la capacidad interactiva del codigo, es posible modificar ciertos parámetros físicos permitiendo explorar una variedad de escenarios del mismo fenómeno.

## Como empezar a usar este repositorio

- Clona este repositorio en tu máquina local utilizando el siguiente comando:
git clone <https://gitlab.com/saraa.carvajal/entregable_1_fc1.git>
cd existing_repo
git remote add origin https://gitlab.com/saraa.carvajal/entregable_1_fc1.git
git branch -M main
git push -uf origin main
- instala las dependencias necesarias ejecutando el siguiente comando en la raíz del proyecto: 
pip install -r requirements.txt

## Resultados: 
Algunos ejemplos de los resultados que puede encontrar se encuentran en la carpeta <results_images>

## Autores y contribución
Este trabajo se realizó en equipo por las siguientes contribuyentes: 
Sara Carvajal (saraa.carvajal@udea.edu.co) , Dayana Henao (dayana.henaoa@udea.edu.co), Valentina Lobo (valentina.lobo@udea.edu.co)
